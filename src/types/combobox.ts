export type ComboboxData = {
  id: number;
  val: string;
};
