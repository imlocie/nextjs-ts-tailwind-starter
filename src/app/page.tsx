import Combobox from "@/components/combobox";
import Transactions from "@/components/transactions";
import { Transaction } from "@/types/transaction";

export default function Home() {
  const transactions: Transaction[] = [
    {
      id: 0,
      name: "Item",
      buyLimit: 1000,
      minPrice: 100,
      maxPrice: 200,
    },
  ];

  return (
    <div className="flex flex-col items-center min-h-screen px-12 py-6 bg-[#2e3440]">
      <p className="text-white">Hello World!</p>
    </div>
  );
}
