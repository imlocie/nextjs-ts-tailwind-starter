"use client";

import { Dispatch, useState } from "react";
import { CheckIcon, ChevronUpDownIcon } from "@heroicons/react/20/solid";
import { Combobox as HeadlessCombobox } from "@headlessui/react";
import { ComboboxData } from "@/types/combobox";

function classNames(...classes: (string | boolean)[]) {
  return classes.filter(Boolean).join(" ");
}

export default function Combobox({
  inputList,
  selectedValue,
  setSelectedValue,
}: {
  inputList: ComboboxData[];
  selectedValue: ComboboxData | null;
  setSelectedValue: Dispatch<ComboboxData | null>;
}) {
  const [query, setQuery] = useState("");

  function getMatchingItemsLimited(limit: number = 10): ComboboxData[] {
    let output: ComboboxData[] = [];
    const queryText = query.toLowerCase();

    if (queryText.length <= 0) {
      return output;
    }

    for (const item of inputList) {
      if (item.val.toLowerCase().includes(queryText)) {
        output.push(item);

        if (output.length > limit) {
          return output;
        }
      }
    }

    return output;
  }

  const filteredList = query === "" ? inputList : getMatchingItemsLimited(10);

  return (
    <HeadlessCombobox
      as="div"
      value={selectedValue}
      onChange={(e) => setSelectedValue(e)}>
      <div className="relative mt-2">
        <HeadlessCombobox.Input
          className="w-full rounded-md border-0 bg-[#eceff4] py-1.5 pl-3 pr-10 text-gray-900 shadow-sm focus:outline-none sm:text-sm sm:leading-6"
          onChange={(event) => setQuery(event.target.value)}
          displayValue={(item: ComboboxData) => item?.val}
        />
        <HeadlessCombobox.Button className="absolute inset-y-0 right-0 flex items-center rounded-r-md px-2 focus:outline-none">
          <ChevronUpDownIcon
            className="h-5 w-5 text-gray-400"
            aria-hidden="true"
          />
        </HeadlessCombobox.Button>

        {filteredList.length > 0 && (
          <HeadlessCombobox.Options className="absolute z-10 mt-1 max-h-60 w-full overflow-auto rounded-md bg-white py-1 text-base shadow-lg focus:outline-none sm:text-sm">
            {filteredList.map((item) => (
              <HeadlessCombobox.Option
                key={item.id}
                value={item}
                className={({ active }) =>
                  classNames(
                    "relative cursor-default select-none py-2 pl-3 pr-9",
                    active ? "bg-indigo-600 text-white" : "text-gray-900"
                  )
                }>
                {({ active, selected }) => (
                  <>
                    <span
                      className={classNames(
                        "block truncate",
                        selected && "font-semibold"
                      )}>
                      {item.val}
                    </span>

                    {selected && (
                      <span
                        className={classNames(
                          "absolute inset-y-0 right-0 flex items-center pr-4",
                          active ? "text-white" : "text-indigo-600"
                        )}>
                        <CheckIcon className="h-5 w-5" aria-hidden="true" />
                      </span>
                    )}
                  </>
                )}
              </HeadlessCombobox.Option>
            ))}
          </HeadlessCombobox.Options>
        )}
      </div>
    </HeadlessCombobox>
  );
}
