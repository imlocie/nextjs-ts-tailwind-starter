NextJS with TypeScript and TailwindCSS setup and basic components

## Getting Started

### Installation

```
pnpm install
```

### Start Local Development Server

```
pnpm dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.
